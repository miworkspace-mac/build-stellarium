#!/bin/bash

NEWLOC=`curl https://api.github.com/repos/Stellarium/stellarium/releases/latest 2>/dev/null | /usr/local/izzy/tools/jq -r '.assets[] | .browser_download_url'| grep macOS | head -1 `

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi